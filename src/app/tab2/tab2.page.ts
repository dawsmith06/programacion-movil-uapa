import { Component, OnInit } from "@angular/core";
import { Geolocation } from '@ionic-native/geolocation/ngx';
import {GoogleMaps,GoogleMap} from "@ionic-native/google-maps";
import { Platform} from "@ionic/angular";

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit{
  map: GoogleMap;

  constructor(private geolocation: Geolocation,private platform: Platform) {
    this.geolocation.getCurrentPosition().then((resp) => {
      alert("Latitude:"+resp.coords.latitude + " Longitude: "+resp.coords.longitude);
     }).catch((error) => {
       console.log('Error getting location', error);
     });
  }

  async ngOnInit() {
    await this.platform.ready();
    await this.downloadMap();
  }

  downloadMap() {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.map = GoogleMaps.create("map_canvas", {
        camera: {
          target: {lat: resp.coords.latitude,lng: resp.coords.longitude},
          zoom: 18,
          tilt: 30
        }
      });
     }).catch((error) => {
       console.log('Error getting location', error);
     });
  }
}
