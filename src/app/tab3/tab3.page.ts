import { Component } from '@angular/core';
import { NativeAudio } from '@ionic-native/native-audio/ngx';
@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  constructor(private nativeAudio: NativeAudio) {
    this.nativeAudio.preloadSimple('uniqueId1', '../assets/test.mp3');
  }

}
