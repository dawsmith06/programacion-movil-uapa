import { Component } from '@angular/core';
import { BLE } from '@ionic-native/ble/ngx';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  devices : any[] = [];

  constructor(private ble:BLE) {
    this.getDevices();
  }

  getDevices(){
    this.ble.scan([],15).subscribe(device => {
      this.devices.push(device);
    });
  }

  sendData(device_id : string){
    var data = new Uint8Array(3);
    data[0] = 0xFF; // red
    data[1] = 0x00; // green
    data[2] = 0xFF; // blue
    this.ble.write(device_id, "FF10", "FF11", data.buffer).then(()=>{
      alert("Datos enviados")
    });
  }

}
